# Petri

# Representación de la red de Petri (Grafo)
Para la representación de la red de Petri escogí representar el flujo de la red por medio de una lista de listas y una lista de estructuras (Estructura ad hoc con presets y postsets)

# Representación del marcado de la red de Petri
Para la representación del marcado escogí entre un mapa y una lista con el marcado actual, la diferencia de estos es que en la lista solo se guardan los lugares activos en el momento mientras que en el mapa se guardan todos los lugares y el numero de fichas que contienen. En mi opinion el mapa podria ser más visual pero al momento de hacer las busquedas seria un mucho más tedioso ya que habria que ir buscando que lugares tienen una o mas fichas, mientras que la lista guarda directamente los lugares que estan activos, por lo cual decidí usar la lista.

# Implementación de la función de ejecución de una transición
Para la implementación de la función tuve que hacer dos modulos diferentes, uno para el caso en el cual la red esta representada como una lista de listas y otra para la estructura ad hoc. Ambas tenian una logica parecida, la cual se basaba en encontrar la transición, obtener su preset y verificar que concordara con el marcado dado, si era asi regresaba el marcado actual (en caso de que algún otro lugar estuviera activado) más el nuevo marcado, de lo contrario regresaba el mismo marcado. A pesar de que seguian una misma logica considero que la implementación para la estructura ad hoc fue más sencilla ya que esa te da los presets y los postsets directamente solo buscando concordar un valor, mientras que en la lista debia de recorrerla por completo e ir guardando los valores para regresarlos.
