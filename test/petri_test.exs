defmodule PetriTest do
  use ExUnit.Case
  doctest Petri

  test "Test de codificación de red de petri" do
    assert Petri.red1() == [
      [:P0, :A],
      [:A, :P1],
      [:A, :P2],
      [:P1, :B],
      [:P1, :D],
      [:P2, :C],
      [:P2, :D],
      [:B, :P3],
      [:C, :P4],
      [:D, :P3],
      [:D, :P4],
      [:P3, :E],
      [:P4, :E],
      [:E, :P5]
    ]
  end

  test "Test 2 de codificación de red de petri" do
    assert Petri.red2() == [
      %Petri{pos: [:P1, :P2], prs: [:P0], val: :A},
      %Petri{pos: [:P3], prs: [:P1], val: :B},
      %Petri{pos: [:P4], prs: [:P2], val: :C},
      %Petri{pos: [:P3,:P4], prs: [:P1,:P2], val: :D},
      %Petri{pos: [:P5], prs: [:P3,:P4], val: :E},
      %Petri{pos: [:A], prs: [nil], val: :P0},
      %Petri{pos: [:B,:D], prs: [:A], val: :P1},
      %Petri{pos: [:C,:D], prs: [:A], val: :P2},
      %Petri{pos: [:E], prs: [:B,:D], val: :P3},
      %Petri{pos: [:E], prs: [:C,:D], val: :P4},
      %Petri{pos: [nil], prs: [:E], val: :P5}
    ]
  end

  test "Test de la función firing con lista de pares" do
    assert ListaPares.firing(Petri.red1,Petri.m5,:C) == [:P3,:P4]
    assert ListaPares.firing(Petri.red1,Petri.m6,:B) == [:P0,:P2,:P3]
  end

  test "Test 1 de la función firing con estructura adhoc" do
    assert EstructA.firing(Petri.red2,Petri.m2,:D) == [:P3,:P4]
    assert EstructA.firing(Petri.red2,Petri.m7,:E) == [:P1,:P5]
  end


  test "Test 1 de enablement con lista de pares" do
    assert ListaPares.enablement(Petri.red1, [:P2,:P4]) == [:C]
    assert ListaPares.enablement(Petri.red1, [:P1,:P2,:P4]) == [:B,:D,:C]
  end


  test "Test 1 de enablement con estructura adhoc" do
    assert EstructA.enablement(Petri.red2, [:P3,:P4]) == [:E]
    assert EstructA.enablement(Petri.red2, [:P0,:P1,:P4]) == [:A,:B]
  end

  test "Test 1 de replaying con lista de pares" do
    assert ListaPares.replaying(Petri.red1,Petri.m0,"log1.txt") == [2, 8]
    assert ListaPares.replaying(Petri.red3,Petri.m0,"log1.txt") == [6, 4]
  end

  test "Test 1 de replaying con estructura adhoc" do
    assert EstructA.replaying(Petri.red2,Petri.m0,"log2.txt") == [2, 8]
    assert EstructA.replaying(Petri.red4,Petri.m0,"log2.txt") == [5, 5]
  end

  test "Test 1 de reachability con lista de pares" do
    assert ListaPares.reachG(Petri.red1,Petri.m0) == [
      %{m0: [:P0], mn: [:P1, :P2], val: :A},
      %{m0: [:P1, :P2], mn: [:P1, :P4], val: :C},
      %{m0: [:P1, :P2], mn: [:P2, :P3], val: :B},
      %{m0: [:P1, :P2], mn: [:P3, :P4], val: :D},
      %{m0: [:P1, :P4], mn: [:P3, :P4], val: :B},
      %{m0: [:P2, :P3], mn: [:P3, :P4], val: :C},
      %{m0: [:P3, :P4], mn: [:P5], val: :E}
    ]

    assert ListaPares.reachG(Petri.red3,Petri.m0) == [
      %{m0: [:P0], mn: [:P1, :P2], val: :A},
      %{m0: [:P1, :P2], mn: [:P1, :P4], val: :C},
      %{m0: [:P1, :P2], mn: [:P2, :P3], val: :B},
      %{m0: [:P1, :P4], mn: [:P1, :P2], val: :D},
      %{m0: [:P1, :P4], mn: [:P3, :P4], val: :B},
      %{m0: [:P2, :P3], mn: [:P3, :P4], val: :C},
      %{m0: [:P3, :P4], mn: [:P2, :P3], val: :D},
      %{m0: [:P3, :P4], mn: [:P5], val: :E}
    ]

    assert ListaPares.reachG(Petri.red5,Petri.m0) == [
      %{m0: [:P0], mn: [:P1, :P2], val: :A},
      %{m0: [:P1, :P2], mn: [:P1, :P4], val: :C},
      %{m0: [:P1, :P2], mn: [:P2, :P3], val: :B},
      %{m0: [:P1, :P2], mn: [:P3], val: :D},
      %{m0: [:P1, :P4], mn: [:P3, :P4], val: :B},
      %{m0: [:P2, :P3], mn: [:P3, :P4], val: :C},
      %{m0: [:P3, :P4], mn: [:P5], val: :E}
    ]
  end


  test "Test 1 de reachability con estructura adhoc" do
    assert EstructA.reachG(Petri.red2,Petri.m0) == [
      %{m0: [:P0], mn: [:P1, :P2], val: :A},
      %{m0: [:P1, :P2], mn: [:P1, :P4], val: :C},
      %{m0: [:P1, :P2], mn: [:P2, :P3], val: :B},
      %{m0: [:P1, :P2], mn: [:P3, :P4], val: :D},
      %{m0: [:P1, :P4], mn: [:P3, :P4], val: :B},
      %{m0: [:P2, :P3], mn: [:P3, :P4], val: :C},
      %{m0: [:P3, :P4], mn: [:P5], val: :E}
    ]

    assert EstructA.reachG(Petri.red4,Petri.m0) == [
      %{m0: [:P0], mn: [:P1, :P2], val: :A},
      %{m0: [:P1, :P2], mn: [:P1, :P4], val: :C},
      %{m0: [:P1, :P2], mn: [:P2, :P3], val: :B},
      %{m0: [:P1, :P4], mn: [:P1, :P2], val: :D},
      %{m0: [:P1, :P4], mn: [:P3, :P4], val: :B},
      %{m0: [:P2, :P3], mn: [:P3, :P4], val: :C},
      %{m0: [:P3, :P4], mn: [:P2, :P3], val: :D},
      %{m0: [:P3, :P4], mn: [:P5], val: :E}
    ]

    assert EstructA.reachG(Petri.red6,Petri.m0) == [
      %{m0: [:P0], mn: [:P1, :P2], val: :A},
      %{m0: [:P1, :P2], mn: [:P1, :P4], val: :C},
      %{m0: [:P1, :P2], mn: [:P2, :P3], val: :B},
      %{m0: [:P1, :P2], mn: [:P3], val: :D},
      %{m0: [:P1, :P4], mn: [:P3, :P4], val: :B},
      %{m0: [:P2, :P3], mn: [:P3, :P4], val: :C},
      %{m0: [:P3, :P4], mn: [:P5], val: :E}
    ]
  end


end
