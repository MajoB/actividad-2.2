defmodule Petri do

  defstruct prs: [nil], val: nil, pos: [nil]
  def trans, do: [:A, :B, :C, :D, :E]
  def marcadoMap, do: %{p0: 1, p1: 0, p2: 0, p3: 0, p4: 0, p5: 0}

  def m0, do: [:P0]
  def m1, do: [:P0,:P1]
  def m2, do: [:P1,:P2]
  def m3, do: [:P3,:P4]
  def m4, do: [:P1,:P4]
  def m5, do: [:P2,:P3]
  def m6, do: [:P0,:P1,:P2]
  def m7, do: [:P1,:P3,:P4]


  def red1 do
    [
      [:P0, :A],
      [:A, :P1],
      [:A, :P2],
      [:P1, :B],
      [:P1, :D],
      [:P2, :C],
      [:P2, :D],
      [:B, :P3],
      [:C, :P4],
      [:D, :P3],
      [:D, :P4],
      [:P3, :E],
      [:P4, :E],
      [:E, :P5]
    ]
  end

  def red2 do
    [
      %Petri{prs: [:P0], val: :A, pos: [:P1, :P2]},
      %Petri{prs: [:P1], val: :B, pos: [:P3]},
      %Petri{prs: [:P2], val: :C, pos: [:P4]},
      %Petri{prs: [:P1,:P2], val: :D, pos: [:P3,:P4]},
      %Petri{prs: [:P3,:P4], val: :E, pos: [:P5]},
      %Petri{val: :P0, pos: [:A]},
      %Petri{prs: [:A], val: :P1, pos: [:B,:D]},
      %Petri{prs: [:A], val: :P2, pos: [:C,:D]},
      %Petri{prs: [:B,:D], val: :P3, pos: [:E]},
      %Petri{prs: [:C,:D], val: :P4, pos: [:E]},
      %Petri{prs: [:E], val: :P5}
    ]
  end

  def red3 do
    [
      [:P0, :A],
      [:A, :P1],
      [:A, :P2],
      [:P1, :B],
      [:P2, :C],
      [:B, :P3],
      [:D, :P2],
      [:C, :P4],
      [:P3, :E],
      [:P4, :E],
      [:P4, :D],
      [:E, :P5]
    ]
  end

  def red4 do
    [
      %Petri{prs: [:P0], val: :A, pos: [:P1, :P2]},
      %Petri{prs: [:P1], val: :B, pos: [:P3]},
      %Petri{prs: [:P2], val: :C, pos: [:P4]},
      %Petri{prs: [:P4], val: :D, pos: [:P2]},
      %Petri{prs: [:P3,:P4], val: :E, pos: [:P5]},
      %Petri{val: :P0, pos: [:A]},
      %Petri{prs: [:A], val: :P1, pos: [:B]},
      %Petri{prs: [:A, :D], val: :P2, pos: [:C]},
      %Petri{prs: [:B], val: :P3, pos: [:E]},
      %Petri{prs: [:C], val: :P4, pos: [:D, :E]},
      %Petri{prs: [:E], val: :P5}
    ]
  end

  def red5 do
    [
      [:P0, :A],
      [:A, :P1],
      [:A, :P2],
      [:P1, :B],
      [:P1, :D],
      [:P2, :C],
      [:P2, :D],
      [:B, :P3],
      [:D, :P3],
      [:C, :P4],
      [:P3, :E],
      [:P4, :E],
      [:E, :P5]
    ]
  end

  def red6 do
    [
      %Petri{prs: [:P0], val: :A, pos: [:P1, :P2]},
      %Petri{prs: [:P1], val: :B, pos: [:P3]},
      %Petri{prs: [:P2], val: :C, pos: [:P4]},
      %Petri{prs: [:P1,:P2], val: :D, pos: [:P3]},
      %Petri{prs: [:P3,:P4], val: :E, pos: [:P5]},
      %Petri{val: :P0, pos: [:A]},
      %Petri{prs: [:A], val: :P1, pos: [:B,:D]},
      %Petri{prs: [:A], val: :P2, pos: [:C, :D]},
      %Petri{prs: [:B, :D], val: :P3, pos: [:E]},
      %Petri{prs: [:C], val: :P4, pos: [:E]},
      %Petri{prs: [:E], val: :P5}
    ]
  end

end

defmodule ListaPares do
  def firing(red1, marca, tran) do
    if ((preset(red1, tran) -- marca) == []) do
      Enum.sort(Enum.uniq(postset(red1, tran) ++ (marca -- preset(red1, tran))))
    else
      marca
    end
  end

  def enablement([], _marca), do: []
  def enablement([h|t], marca) do
    if((preset(Petri.red1, hd(tl(h))) -- marca) == []) do
      Enum.uniq([hd(tl(h))] ++ enablement(t, marca))
    else
      enablement(t, marca)
    end
  end

  def replayLine(_red3, _marca, []), do: 1
  def replayLine(red3, marca, [h|t]) do
    newMark = firing(red3,marca,String.to_atom(h))
    if (newMark == marca) do
      0
    else
      replayLine(red3,newMark,t)
    end
  end

  def replayList(_red3, _marca, []), do: 0
  def replayList(red3, marca, [h|t]) do
    replayLine(red3, marca, h) + replayList(red3, marca, t)
  end

  def replaying(red3, marca, doc) do
    list =
    File.read!(doc)
    |> String.split
    |> Enum.map(fn line -> String.split(line, ",") end)
    t = replayList(red3,marca,list)
    [t] ++ [length(list) - t]
  end

  def nodos(_red,_marca,_ant,[]), do: []
  def nodos(red,marca,ant,tr) do
    [h|t] = tr;
    if(((preset(red,h)) -- marca) == []) do
      new = firing(red, marca, h)
      if(ant != new) do
        Enum.uniq([%{m0: marca, val: h, mn: new}] ++ nodos(red,marca,ant, t) ++ nodos(red,new,marca,Petri.trans))
      else
        Enum.uniq([%{m0: marca, val: h, mn: new}] ++ nodos(red,marca,ant, t))
      end
    else
      nodos(red, marca,ant, t)
    end
  end

  def reachG(red5, marca) do
    Enum.sort(nodos(red5, marca,[], Petri.trans))
  end

  def preset([],_tran), do: []
  def preset(red, tran) do
    [head|tail] = red
      if (hd(tl(head)) == tran) do
        [hd(head)] ++ preset(tail, tran)
      else
        preset(tail, tran)
      end
  end

  def postset([],_tran), do: []
  def postset(red, tran) do
    [head|tail] = red
    if (hd(head) == tran) do
      [hd(tl(head))] ++ postset(tail, tran)
    else
      postset(tail, tran)
    end
  end

end

defmodule EstructA do
  def firing(red2, marca, tran) do
    nodo = find(red2, tran)
    if ( (nodo.prs -- marca) == []) do
      Enum.sort(Enum.uniq(nodo.pos ++ (marca -- nodo.prs)))
    else
      marca
    end
  end

  def enablement([], _marca), do: []
  def enablement([h|t], marca) do
    if((h.prs -- marca) == []) do
      [h.val] ++ enablement(t, marca)
    else
      enablement(t, marca)
    end
  end

  def replayLine(_red4, _marca, []), do: 1
  def replayLine(red4, marca, [h|t]) do
    newMark = firing(red4,marca,String.to_atom(h))
    if (newMark == marca) do
      0
    else
      replayLine(red4,newMark,t)
    end
  end

  def replayList(_red4, _marca, []), do: 0
  def replayList(red4, marca, [h|t]) do
    replayLine(red4, marca, h) + replayList(red4, marca, t)
  end

  def replaying(red4, marca, doc) do
    list =
    File.read!(doc)
    |> String.split
    |> Enum.map(fn line -> String.split(line, ",") end)
    t = replayList(red4,marca,list)
    [t] ++ [length(list) - t]
  end

  def nodos(_red,_marca,_ant,[]), do: []
  def nodos(red,marca, ant,tr) do
    [h|t] = tr
    ts = find(red, h)
    if(( ts.prs -- marca) == []) do
      new = firing(red, marca, h)
      if(ant != new) do
        Enum.uniq([%{m0: marca, val: h, mn: new}] ++ nodos(red,marca,ant, t) ++ nodos(red,new,marca,Petri.trans))
      else
        Enum.uniq([%{m0: marca, val: h, mn: new}] ++ nodos(red,marca,ant, t))
      end
    else
      nodos(red, marca,ant, t)
    end
  end

  def reachG(red6, marca) do
    Enum.sort(nodos(red6, marca,[], Petri.trans))
  end


  def find([head|tail], tran) do
    if(head == []) do
      %Petri{}
    else
      if (head.val == tran) do
        head
      else
        find(tail, tran)
      end
    end
  end

end
