# Petri

## Petri module
A traves de este modulo se podra acceder a las transciciones, diferentes marcados y las diferentes redes.
Dentro de esta se define la estructura adhoc la cual guarda el preset, el valor y el postset de una transición.
defstruct prs: [nil], val: nil, pos: [nil]
Petri.trans: Te permite obtener la lista de transiciones existentes
Petri.marcadoMap: Te regresa el marcado en manera de mapa
Petri.m0: Te regresa el marcado [:P0] en manera de lista
Petri.m1: Te regresa el marcado [:P0,:P1] en manera de lista
Petri.m2: Te regresa el marcado [:P1,:P2] en manera de lista
Petri.m3: Te regresa el marcado [:P3,:P4] en manera de lista
Petri.m4: Te regresa el marcado [:P1,:P4] en manera de lista
Petri.m5: Te regresa el marcado [:P2,:P3] en manera de lista
Petri.m6: Te regresa el marcado [:P0,:P1,:P2] en manera de lista
Petri.m6: Te regresa el marcado [:P1,:P3,:P4] en manera de lista
Petri.red1: Representa la primera red de petri de la tarea en manera de una lista de listas que representa el flujo de la red
Petri.red2: Representa la primera red de petri de la tarea en manera de una lista de estructura
Petri.red3: Representa la segunda red de petri de la tarea en manera de una lista de listas que representa el flujo de la red
Petri.red4: Representa la segunda red de petri de la tarea en manera de una lista de estructura
Petri.red5: Representa la tercera red de petri de la tarea en manera de una lista de listas que representa el flujo de la red
Petri.red6: Representa la tercera red de petri de la tarea en manera de una lista de estructura

## ListaPares module
A traves de este modulo se podra acceder a las funciones de las redes de petri que son representadas por medio de una lista de listas 

ListaPares.firing: Te permite obtener un nuevo marcado a partir de  un marcado actual y la transicion que se desea usar. Recibe la red, el marcado y la transición, regresa una lista con el nuevo marcado.
ListaPares.enablement: Te permite obtener las transiciones que son activadas a partir de un cierto marcado. Recibe la red y el marcado, regresa una lista con las transiciones que se activan.
ListaPares.replaying: Te permite leer un archivo que contiene pruebas de posbles "caminos" en la red. Recibe la red, el marcado inicial y el nombre del archivo que contiene la bitacora de ejecución, regresa el numero de pruebas pasadas y el numero de pruebas que no.
ListaPares.reachG: Te permite conocer todos los marcados posibles asociados a la red. Recibe la red y el marcado inicial, regresa una lista de mapas que contienen el marcado actual, la transicion y el nuevo marcado.
ListaPares.replayLine: Función auxiliar de replaying que nos permite checar los valores de una lista que esta dentro de una lista de listas. Recibe la red, el marcado y una lista.
ListaPares.replayList: Función auxiliar de replaying que nos permite checar todos los valores de una lista. Recibe la red, el marcado y una lista.
ListaPares.nodos: Función auxiliar de reachG que te permite armar la lista de mapas checando los viejos y nuevos marcados. Recibe la red, el marcado actual, el marcado anterior y una lista de transiciones.
ListaPares.preset: Función que te permite obtener el preset de una transición. Recibe la red y la transición deseada.
ListaPares.postset: Función que te permite obtener el postset de una transición. Recibe la red y la transición deseada.

## EstructA module
A traves de este modulo se podra acceder a las funciones de las redes de petri que son representadas por medio de una lista de estructuras

EstructA.firing: Te permite obtener un nuevo marcado a partir de  un marcado actual y la transicion que se desea usar. Recibe la red, el marcado y la transición, regresa una lista con el nuevo marcado.
EstructA.enablement: Te permite obtener las transiciones que son activadas a partir de un cierto marcado. Recibe la red y el marcado, regresa una lista con las transiciones que se activan.
EstructA.replaying: Te permite leer un archivo que contiene pruebas de posbles "caminos" en la red. Recibe la red, el marcado inicial y el nombre del archivo que contiene la bitacora de ejecución, regresa el numero de pruebas pasadas y el numero de pruebas que no.
EstructA.reachG: Te permite conocer todos los marcados posibles asociados a la red. Recibe la red y el marcado inicial, regresa una lista de mapas que contienen el marcado actual, la transicion y el nuevo marcado.
EstructA.replayLine: Función auxiliar de replaying que nos permite checar los valores de una lista que esta dentro de una lista de listas. Recibe la red, el marcado y una lista.
EstructA.replayList: Función auxiliar de replaying que nos permite checar todos los valores de una lista. Recibe la red, el marcado y una lista.
EstructA.nodos: Función auxiliar de reachG que te permite armar la lista de mapas checando los viejos y nuevos marcados. Recibe la red, el marcado actual, el marcado anterior y una lista de transiciones.
EstructA.find: Función que te permite encontrar la estructura de una transición en especifico. Recibe la red y la transicion buscada, regresa la estructura con los datos de la transición.
